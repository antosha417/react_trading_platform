import React, { useState } from "react";
import Calendar from "./timepicker.jsx"
import ButtonForCalendar from "./button_for_calendar.jsx"

const ContainerForCalendar = props => {

    const [displayCalendar, setDisplayCalendar] = useState(false);


    return (
        <>
            {(displayCalendar) ? <Calendar /> : <></>}
            < ButtonForCalendar displayCalendar={displayCalendar}
                setDisplayCalendar={setDisplayCalendar} />
        </>

    );
};

export default ContainerForCalendar