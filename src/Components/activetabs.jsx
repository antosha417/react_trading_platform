import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Figure from "react-bootstrap/Figure";

const BuySaleButton = () => (
    <Tabs defaultActiveKey="plots"
        id="tab1">
        <Tab eventKey="buy" title="Buy">
            <p>This is the buy plot</p>
            <Figure>         {/*this is going to be Stefan's diagram*/}
                <Figure.Image
                    width={171}
                    height={180}
                    alt="171x180"
                    src="holder.js/171x180" />
                <Figure.Caption>
                    Price over time
                </Figure.Caption>
            </Figure>
        </Tab>

        <Tab eventKey="sale" title="Sale" >
            <p>This is the sale plot</p>
            <Figure>         {/*this is going to be Stefan's diagram*/}
                <Figure.Image
                    width={171}
                    height={180}
                    alt="171x180"
                    src="holder.js/171x180" />
                <Figure.Caption>
                    Price over time
                </Figure.Caption>
            </Figure>
        </Tab>

        <Tab eventKey="both" title="Both">
            <p>Both plots are shown</p>
            <Figure>         {/*this is going to be Stefan's diagram*/}
                <Figure.Image
                    width={171}
                    height={180}
                    alt="171x180"
                    src="holder.js/171x180" />
                <Figure.Caption>
                    Price over time
                </Figure.Caption>
            </Figure>
        </Tab>
    </Tabs >
)

export default BuySaleButton