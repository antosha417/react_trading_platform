import React, { useState } from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button'

function AlertDismissible() {
    const [show, setShow] = useState(true);

    return (
        <>
            <Alert show={show} variant="success">
                <Alert.Heading>Have a good trades, Honey!</Alert.Heading>
                <p>
                    You loged in successfully, congrats.
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={() => setShow(false)} variant="outline-success">
                        Close me ya'll!
          </Button>
                </div>
            </Alert>
        </>
    );
}

export default AlertDismissible;