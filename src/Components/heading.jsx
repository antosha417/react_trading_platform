import React from 'react';
import Media from 'react-bootstrap/Media';
import Row from 'react-bootstrap/Row'
import AlertDismissible from './Alert'

const Heading = () => (

    <Media>
        <Row>
            <Media.Body>
                <h2>B.B.</h2>
                <p>
                    You show the vision, we create it
                </p>
            </Media.Body>
        </Row>
        <Row>
            <AlertDismissible />
        </Row>

    </Media>

)

export default Heading