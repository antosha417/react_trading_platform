import React from "react";
import Button from "react-bootstrap/Button";

const ButtonForCalendar = (props) => {

  const DisplayCalendar = () => {
    props.setDisplayCalendar(!props.displayCalendar)
  }

  return (
    <Button onClick={DisplayCalendar} >
      {(props.displayCalendar) ? 'Hide' : ''} calendar
        </Button>


  );
};

export default ButtonForCalendar