import React, { useState } from 'react';

import HistoricLivePlots from "./Plots/HistoricLivePlots.jsx";
import { Container } from 'react-bootstrap';
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Heading from "./heading.jsx"
import ContainerForCalendar from "./calendar_with_button_lalala.jsx"
import InstrumentsButton from './instruments'
import CtpyButton from './ctpy'
import './bb.jpg'
import Button from 'react-bootstrap/Button'


const Main = props => {

    const [instrument, setInstrument] = useState({ name: 'Instrument', iid: 0 });
    const [ctpy, setCtpy] = useState({ name: 'Ctpy', iid: 0 });
    const [filter, setFilter] = useState({ instrument_id: instrument.iid, ctpy_id: ctpy.iid })

    return (
        <Container>
            <Heading />
            <Row>
                <Col lg={{ span: 8, offset: 2 }}>

                    <HistoricLivePlots filter={filter} />
                    <Row>
                        <Col>

                            <ContainerForCalendar />
                        </Col>
                        <Col>
                            <InstrumentsButton
                                instrument={instrument}
                                setInstrument={setInstrument} />
                        </Col>
                        <Col>
                            <CtpyButton ctpy={ctpy} setCtpy={setCtpy} />
                        </Col>
                        <Col>
                            <Button variant="success"
                                onClick={() => setFilter({ instrument_id: instrument['iid'], ctpy_id: ctpy['iid'] })}>
                                Apply
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Button variant="link" href='http://communicator-traiding.apps.dbgrads-6eec.openshiftworkshop.com/db_status'>
                    Check DB satuts
               </Button>
            </Row>
        </Container >

    );
};

export default Main
