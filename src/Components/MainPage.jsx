import React from "react";
import Main from "./Main"

const MainPage = props => {
    return (
        <>
            {(props.authorized) ? <Main /> : <></>}
        </>
    );
};

export default MainPage