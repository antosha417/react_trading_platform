const filterDeal = (deal, filter_obj) => {
    for (var key in filter_obj) {
        if (filter_obj[key] !== 0 && deal[key] !== filter_obj[key]) {
            return null
        }
    }
    return deal
}

export default filterDeal