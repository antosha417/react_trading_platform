import React, { useState } from 'react'
import { Line } from 'react-chartjs-2';

import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';

import filterDeal from './filter'

const stringObservable = Observable.create(observer => {
    const source = new EventSource('http://communicator-traiding.apps.dbgrads-6eec.openshiftworkshop.com/get_data');
    source.addEventListener('message', (messageEvent) => {
        console.log(messageEvent);
        observer.next(messageEvent.data);
    }, false);
});

const MAX_POINTS = 50;

const LiveLinePlot = props => {
    const [data, setData] = useState(
        {
            labels: Array.from(Array(MAX_POINTS), () => ''),
            datasets: [
                {
                    label: 'Buy',
                    lineTension: 0.2,
                    borderColor: 'rgba(242, 38, 19, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBaccdkgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: Array.from(Array(MAX_POINTS), () => 0)
                },
                {
                    label: 'Sell',
                    fill: false,
                    lineTension: 0.2,
                    backgroundColor: 'rgba(31, 58, 147, 1)',
                    borderColor: 'rgba(75,45,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBaccdkgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: Array.from(Array(MAX_POINTS), () => 0)
                }
            ]
        }
    )

    useObservable(
        state =>
            stringObservable.pipe(
                withLatestFrom(state),
                map(([state]) => {

                    let buy_prices = [...data.datasets[0].data];
                    let sell_prices = [...data.datasets[1].data];

                    let deal = JSON.parse(state);
                    deal = filterDeal(deal, props.filter);
                    console.log(deal)
                    console.log(props.filter)
                    if (!deal) {
                        return state;
                    }
                    if ('B' === deal.type) {
                        buy_prices.push(deal.price)
                    } else {
                        sell_prices.push(deal.price)
                    }
                    if (buy_prices.length >= MAX_POINTS) {
                        buy_prices.shift();
                    }
                    if (sell_prices.length >= MAX_POINTS) {
                        sell_prices.shift()
                    }
                    setData(
                        {
                            datasets: [
                                {
                                    label: 'Buy',
                                    data: buy_prices,
                                },
                                {
                                    label: 'Sell',
                                    data: sell_prices,
                                }
                            ]
                        }
                    );
                    return state;
                })
            )
    );


    return (
        <div>
            <h2 align="center">BB Chart</h2>
            <Line data={data} />
        </div>
    );;
};


export default LiveLinePlot;