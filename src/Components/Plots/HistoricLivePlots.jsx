import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import LiveLinePlot from './LiveLinePlot'
import HistoricLinePlot from './HistoricLinePlot'


const HistoricLivePlots = (props) => (
    <Tabs defaultActiveKey="live">
        <Tab eventKey="historic" title="Historic">
            <HistoricLinePlot />
        </Tab>

        <Tab eventKey="live" title="Live" >
            <LiveLinePlot filter={props.filter} />
        </Tab>
    </Tabs >
)

export default HistoricLivePlots