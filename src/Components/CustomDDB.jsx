import React from "react";

import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';

const CustomDDB = () => (
    <DropdownButton id="dropdown-basic-button" title="Instrument">
        <Dropdown.Item href="#/action-1">AAA</Dropdown.Item>
        <Dropdown.Item href="#/action-2">BBB</Dropdown.Item>
        <Dropdown.Item href="#/action-3">CCC</Dropdown.Item>
    </DropdownButton>
)

export default CustomDDB
