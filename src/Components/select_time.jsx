import React from 'react';
import { LinkedCalendar } from 'rb-datepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';

const ExampleComponent = () => {

    const onDatesChange = ({ startDate, endDate }) => {
        console.log(({ startDate, endDate }));
    }

    return (
        <LinkedCalendar onDatesChange={onDatesChange} showDropdowns={false} />
    )

}

export default ExampleComponent