import React from 'react'

import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';



const INSTRUMENTS = { "Instrument": 0, "Astronomica": 1, "Borealis": 2, "Celestial": 3, "Deuteronic": 4, "Eclipse": 5, "Floral": 6, "Galactia": 7, "Heliosphere": 8, "Interstella": 9, "Jupiter": 10, "Koronis": 11, "Lunatic": 12 }

const InstrumentsButton = props => {

    return (
        <>
            <DropdownButton id="dropdown-basic-button" title={props.instrument.name}>
                {
                    Object.keys(INSTRUMENTS).map((name, index) =>
                        <Dropdown.Item key={index}
                            onSelect={() => props.setInstrument({ name: name, iid: INSTRUMENTS[name] })}>
                            {name}
                        </Dropdown.Item>
                    )
                }
            </DropdownButton>
        </>
    );
};


export default InstrumentsButton;