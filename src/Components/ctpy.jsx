import React from 'react'

import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';



const CTPY = { "Ctpy": 0, "Lewis": 1, "Selvyn": 2, "Richard": 3, "Lina": 4, "John": 5, "Nidia": 6 }

const CtpyButton = props => {

    return (
        <>
            <DropdownButton id="dropdown-basic-button" title={props.ctpy.name} >
                {
                    Object.keys(CTPY).map((name, index) =>
                        <Dropdown.Item key={index}
                            onSelect={() => props.setCtpy({ name: name, iid: CTPY[name] })}>
                            {name}
                        </Dropdown.Item>
                    )
                }
            </DropdownButton>
        </>
    );
};


export default CtpyButton;