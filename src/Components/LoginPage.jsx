import React from "react";
import Login from './Login'

const LoginPage = props => {
    return (
        <>
            {(props.authorized) ? <></> : <Login setAuthorized={props.setAuthorized} />}
        </>
    );
};

export default LoginPage