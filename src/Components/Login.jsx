import React, { useState } from "react";
import axios from 'axios'

const Login = props => {
    const [login, setLogin] = useState([]);
    const [password, setPassword] = useState([]);

    const checkCredentials = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.get('http://datagen-traiding.apps.dbgrads-6eec.openshiftworkshop.com/check_password',
                {
                    params: {
                        login,
                        password,
                    }
                });

            const credentialsCheck = await response.data;
            console.log(credentialsCheck)
            props.setAuthorized(credentialsCheck.success)

        } catch (e) {
            props.setAuthorized(e.message);
        }
    }

    return (
        <div>
            <form >
                <div className="form-group">
                    <label htmlFor="todoDescription">You need to login to see:</label>
                    <input
                        type="text"
                        name="login"
                        placeholder="Selvyn"
                        className="form-control"
                        value={login}
                        onChange={e => setLogin(e.target.value)}

                    />
                    <input
                        type="password"
                        name="password"
                        placeholder="password123"
                        className="form-control"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <input type="submit" value="Submit" className="btn btn-primary" onClick={checkCredentials} />
                </div>

            </form>
        </div>
    );
};

export default Login