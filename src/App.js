import React, { useState } from 'react';

import LoginPage from './Components/LoginPage'
import MainPage from "./Components/MainPage"

function App() {

  const [authorized, setAuthorized] = useState(false);

  return (
    <>
      < LoginPage authorized={authorized} setAuthorized={setAuthorized} />
      < MainPage authorized={authorized} />
    </>
  );
}

export default App
